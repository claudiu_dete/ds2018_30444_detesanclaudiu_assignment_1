package dao;

import entities.City;
import org.hibernate.*;

import java.util.List;

import static utils.Constants.FIND_CITY;

public class CityDAO {

    private SessionFactory sessionFactory;

    public CityDAO(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public City findCityByName(String name) {
        Transaction tx = null;
        City city = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            Query query = session.createQuery(FIND_CITY);
            query.setParameter("name", name);
            city = (City) query.uniqueResult();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }
        return city;
    }

    public List<City> findAll(){
        Transaction tx= null;
        List<City> cities=null;
        try(Session session = sessionFactory.openSession()){
            tx=session.beginTransaction();
             cities= session.createCriteria(City.class).list();
             tx.commit();
        } catch(HibernateException e){
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }
        return cities;
    }
}
