package dao;

import entities.User;
import org.hibernate.*;

import static utils.Constants.FIND_BY_USERNAME_PASSWORD;

public class UserDAO {

    private SessionFactory sessionFactory;

    public UserDAO(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public int createUser(User user) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        int userId = -1;
        try {
            tx = session.beginTransaction();
            userId = (Integer) session.save(user);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return userId;
    }

    public User findByUsernameAndPassword(String username, String password) {
        Transaction tx = null;
        User user = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            Query select = session.createQuery(FIND_BY_USERNAME_PASSWORD);
            select.setParameter("username", username);
            select.setParameter("password", password);
            user = (User) select.uniqueResult();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }
        return user;
    }
}
