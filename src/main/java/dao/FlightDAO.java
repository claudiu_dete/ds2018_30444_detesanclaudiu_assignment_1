package dao;


import entities.Flight;
import org.hibernate.*;

import java.util.List;

import static utils.Constants.*;

public class FlightDAO {

    private SessionFactory sessionFactory;

    public FlightDAO(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public int createFlight(Flight flight) {
        Transaction tx = null;
        int flightId = -1;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            flightId = (Integer) session.save(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }
        return flightId;
    }

    public List<Flight> listFlights() {
        Transaction tx = null;
        List<Flight> flights = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            flights = session.createCriteria(Flight.class).list();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }
        return flights;
    }

    public void deleteFlight(int flightNumber) {
        Transaction tx = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            Query query = session.createQuery(DELETE_FLIGHT);
            query.setParameter("flightNumber", flightNumber);
            query.executeUpdate();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }
    }

    public void updateFlight(Flight flight) {
        Transaction tx = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.update(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }
    }

    public Flight findFlightByNumber(int flightNumber) {
        Transaction tx = null;
        Flight flight = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            Query query = session.createQuery(FIND_BY_NUMBER);
            query.setParameter("flight_number", flightNumber);
            flight = (Flight) query.uniqueResult();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }
        return flight;
    }
}
