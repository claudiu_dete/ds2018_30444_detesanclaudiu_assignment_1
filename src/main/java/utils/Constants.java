package utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class Constants {

    public static final String DELETE_FLIGHT = "DELETE FROM Flight WHERE flight_number= :flightNumber";
    public static final String FIND_BY_NUMBER = "from Flight WHERE flight_number = :flight_number";
    public static final String FIND_CITY = "from City WHERE name= :name";
    public static final String FIND_BY_USERNAME_PASSWORD = "from User WHERE username= :username" +
            " AND password= :password";

    public static final String CREATE = "Create";
    public static final String DELETE = "Delete";
    public static final String UPDATE = "Update";
    public static final String LOGOUT = "LogOut";
    public static final String QUERY = "query";

    public static final String ADMIN = "Administrator";
    public static final String TRUE = "True";
    public static final String FALSE = "False";
    public static final String EMPTY = "";

    public static final String WEB_URL = "http://api.geonames.org/timezone?";
    public static final String WEB_USERNAME = "&username=claudiudet";
    public static final String TIME_MARKUP = "<time>";

    public static Timestamp parseDateTime(String date) {
        try {
            date = date.replace("T", " ");
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm");
            Date time = format.parse(date);
            return new Timestamp(time.getTime());
        } catch (ParseException e) {
            System.out.println("Parsing error");
        }
        return null;
    }

    public static boolean isLogged(HttpServletRequest request, String value) {
        boolean isLogged = false;
        Cookie cookies[] = request.getCookies();
        for (int i = 0; i < cookies.length; i++) {
            if (ADMIN.equals(cookies[i].getName()) && value.equals(cookies[i].getValue())) {
                isLogged = true;
            }
        }
        return isLogged;
    }
}
