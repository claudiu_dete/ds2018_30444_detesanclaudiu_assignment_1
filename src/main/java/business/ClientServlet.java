package business;

import dao.CityDAO;
import dao.FlightDAO;
import entities.City;
import entities.Flight;
import entities.User;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import static utils.Constants.*;

@WebServlet("/Client")
public class ClientServlet extends HttpServlet {

    private FlightDAO flightDAO;
    private CityDAO cityDAO;

    public void init() {
        Configuration configuration = new Configuration().configure("hibernate.cfg.xml");
        configuration.addAnnotatedClass(User.class);
        configuration.addAnnotatedClass(City.class);
        configuration.addAnnotatedClass(Flight.class);
        ServiceRegistry serviceRegistry
                = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties()).build();
        SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        this.flightDAO = new FlightDAO(sessionFactory);
        this.cityDAO = new CityDAO(sessionFactory);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (!isLogged(request, FALSE)) {
            response.sendRedirect("/login.html");
            return;
        }
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        printPage(out);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(QUERY.equals(request.getParameter("action"))) {
            try {
                String cityName = request.getParameter("city");
                City city = cityDAO.findCityByName(cityName);
                URL location = new URL(WEB_URL + "lat=" + city.getLatitude() + "&lng=" +
                        city.getLongitude() + WEB_USERNAME);

                URLConnection connection = location.openConnection();
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()));
                String inputLine;
                String message = "";

                while ((inputLine = in.readLine()) != null) {
                    if (inputLine.contains(TIME_MARKUP)) {
                        message = inputLine.split(">")[1].split("<")[0];
                    }
                }
                PrintWriter out = response.getWriter();
                printPage(out);
                out.println("<h1>The local time of the selected city is: " + message + "</h1>");
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            processLogOut(response);
            response.sendRedirect("/login.html");
        }

    }

    private void printPage(PrintWriter out) {
        out.println("<h1> The current available flights are:</h1>");
        out.println("<table style=\"width:100%\">");
        out.println("<tr>");
        out.println("<th> Flight Number </th>");
        out.println("<th> Airplane type </th>");
        out.println("<th> Arrival time </th>");
        out.println("<th> Arrival city </th>");
        out.println("<th> Departure time </th>");
        out.println("<th> Departure city </th>");
        out.println("</tr>");
        List<Flight> flights = flightDAO.listFlights();
        for (Flight flight : flights) {
            out.println("<tr>");
            out.println("<th>" + flight.getFlightNumber() + " </th>");
            out.println("<th>" + flight.getAirplaneType() + " </th>");
            out.println("<th>" + flight.getArrivalTime() + " </th>");
            out.println("<th>" + flight.getArrivalCity().getName() + " </th>");
            out.println("<th>" + flight.getDepartureTime() + " </th>");
            out.println("<th>" + flight.getDepartureCity().getName() + " </th>");
            out.println("</tr>");
        }
        out.println("</table><br><br>");
        out.println("<form method=\"POST\">");
        out.println("<select name=\"city\">");
        List<City> cities = cityDAO.findAll();
        for (City city : cities) {
            out.println("<option name=\"City\" value=\"" + city.getName() + "\">");
            out.println(city.getName() + "</option>");
        }
        out.println("</select>");
        out.println("<button type=\"submit\" name=\"action\" value=\"query\">Submit</button><br>");
        out.println("<button type=\"submit\" name=\"action\" value=\"LogOut\">Log out</button><br>");
        out.println("</form>");

    }

    private void processLogOut(HttpServletResponse response){
        Cookie cookie = new Cookie(ADMIN, EMPTY);
        cookie.setMaxAge(0);
        response.addCookie(cookie);
    }
}