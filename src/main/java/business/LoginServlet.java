package business;

import dao.UserDAO;
import entities.Role;
import entities.User;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static utils.Constants.ADMIN;
import static utils.Constants.FALSE;
import static utils.Constants.TRUE;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {

    private UserDAO userDAO;

    public void init() {
        Configuration configuration = new Configuration().configure("hibernate.cfg.xml");
        configuration.addAnnotatedClass(User.class);
        ServiceRegistry serviceRegistry
                = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties()).build();
        SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        this.userDAO = new UserDAO(sessionFactory);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        User loggedUser = userDAO.findByUsernameAndPassword(request.getParameter("username")
                , request.getParameter("password"));
        if (loggedUser == null) response.sendRedirect("/login.html");
        else if (loggedUser.getRole() == Role.CLIENT) {
            response.addCookie(new Cookie(ADMIN, FALSE));
            response.sendRedirect("/Client");
        } else {
            response.addCookie(new Cookie(ADMIN,TRUE));
            response.sendRedirect("/Administrator");
        }
    }
}
