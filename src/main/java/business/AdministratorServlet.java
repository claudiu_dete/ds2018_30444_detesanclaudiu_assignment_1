package business;

import dao.CityDAO;
import dao.FlightDAO;
import entities.City;
import entities.Flight;
import entities.User;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.List;

import static utils.Constants.*;

@WebServlet("/Administrator")
public class AdministratorServlet extends HttpServlet {

    private FlightDAO flightDAO;
    private CityDAO cityDAO;

    public void init() {
        Configuration configuration = new Configuration().configure("hibernate.cfg.xml");
        configuration.addAnnotatedClass(User.class).addAnnotatedClass(City.class).addAnnotatedClass(Flight.class);
        ServiceRegistry serviceRegistry
                = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties()).build();
        SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        this.flightDAO = new FlightDAO(sessionFactory);
        this.cityDAO = new CityDAO(sessionFactory);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        if (CREATE.equals(action)) {
            processCreateRequest(request);
        } else if (DELETE.equals(action)) {
            processDeleteRequest(request);
        } else if (UPDATE.equals(action)) {
            processUpdateRequest(request);
        } else if (LOGOUT.equals(action)) {
            processLogOutRequest(response);
            response.sendRedirect("/login.html ");
            return;
        }
        response.sendRedirect("/Administrator");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (!isLogged(request, TRUE)) {
            response.sendRedirect("/login.html");
            return;
        }
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        List<Flight> flights = flightDAO.listFlights();
        printFlights(out, flights);
        printForms(out);

    }

    private void processUpdateRequest(HttpServletRequest request) {
        int flightNumber = Integer.parseInt(request.getParameter("flightNumber"));
        Flight flight = flightDAO.findFlightByNumber(flightNumber);
        if (flight != null) {
            String airplaneType = request.getParameter("airplaneType");
            City departureCity = cityDAO.findCityByName(request.getParameter("departureCity"));
            if(departureCity == null){
                departureCity=new City(request.getParameter("departureCity"),50,50);
            }
            City arrivalCity = cityDAO.findCityByName(request.getParameter("arrivalCity"));
            if(arrivalCity ==null){
                arrivalCity = new City(request.getParameter("arrivalCity"),50,50);
            }
            Timestamp arrival = parseDateTime(request.getParameter("arrivalTime"));
            Timestamp departure = parseDateTime(request.getParameter("departureTime"));
            flight.setAirplaneType(airplaneType);
            flight.setDepartureCity(departureCity);
            flight.setArrivalCity(arrivalCity);
            flight.setArrivalTime(arrival);
            flight.setDepartureTime(departure);
            flightDAO.updateFlight(flight);
        }
    }

    private void processDeleteRequest(HttpServletRequest request) {
        int flightNumber = Integer.parseInt(request.getParameter("flightNumber"));
        flightDAO.deleteFlight(flightNumber);

    }

    private void processCreateRequest(HttpServletRequest request) throws NumberFormatException {
        int flightNumber = Integer.parseInt(request.getParameter("flightNumber"));
        if(flightDAO.findFlightByNumber(flightNumber)==null) {
            String airplaneType = request.getParameter("airplaneType");
            City departureCity = cityDAO.findCityByName(request.getParameter("departureCity"));
            if (departureCity == null) {
                departureCity = new City(request.getParameter("departureCity"), 50, 50);
            }
            City arrivalCity = cityDAO.findCityByName(request.getParameter("arrivalCity"));
            if (arrivalCity == null) {
                arrivalCity = new City(request.getParameter("arrivalCity"), 50, 50);
            }
            Timestamp arrival = parseDateTime(request.getParameter("arrivalTime"));
            Timestamp departure = parseDateTime(request.getParameter("departureTime"));
            Flight flight = new Flight(flightNumber, airplaneType, departureCity, departure, arrivalCity, arrival);
            flightDAO.createFlight(flight);
        }
    }

    private void processLogOutRequest(HttpServletResponse response) {
        Cookie cookie = new Cookie(ADMIN, EMPTY);
        cookie.setMaxAge(0);
        response.addCookie(cookie);
    }

    private void printForms(PrintWriter out) {
        out.println("<form method=\"POST\">");
        out.println("Flight number:<br>\n" +
                "  <input type=\"number\" name=\"flightNumber\"><br>");
        out.println("Airplane type:<br>\n" +
                "  <input type=\"text\" name=\"airplaneType\"><br>");
        out.println("Departure city:<br>\n" +
                "  <input type=\"text\" name=\"departureCity\"><br>");
        out.println("Departure time:<br>\n" +
                "  <input type=\"datetime-local\" name=\"departureTime\"><br>");
        out.println("Arrival city:<br>\n" +
                "  <input type=\"text\" name=\"arrivalCity\"><br>");
        out.println("Arrival Time:<br>\n" +
                "  <input type=\"datetime-local\" name=\"arrivalTime\"><br>");
        out.println("<input type=\"submit\" name=\"action\" value=\"Create\"><br><br>");
        out.println("<input type=\"submit\" name=\"action\" value=\"Delete\"><br><br>");
        out.println("<input type=\"submit\" name=\"action\" value=\"Update\"><br><br>");
        out.println("<input type=\"submit\" name=\"action\" value=\"LogOut\"><br><br>");
        out.println("</form>");

    }

    private void printFlights(PrintWriter out, List<Flight> flights) {
        out.println("<h1> The current available flights are:</h1>");
        out.println("<table style=\"width:100%\">");
        out.println("<tr>");
        out.println("<th> Flight Number </th>");
        out.println("<th> Airplane type </th>");
        out.println("<th> Arrival time </th>");
        out.println("<th> Arrival city </th>");
        out.println("<th> Departure time </th>");
        out.println("<th> Departure city </th>");
        out.println("</tr>");
        for (Flight flight : flights) {
            out.println("<tr>");
            out.println("<th>" + flight.getFlightNumber() + " </th>");
            out.println("<th>" + flight.getAirplaneType() + " </th>");
            out.println("<th>" + flight.getArrivalTime() + " </th>");
            out.println("<th>" + flight.getArrivalCity().getName() + " </th>");
            out.println("<th>" + flight.getDepartureTime() + " </th>");
            out.println("<th>" + flight.getDepartureCity().getName() + " </th>");
            out.println("</tr>");
        }
        out.println("</table>");
    }


}
