import dao.FlightDAO;
import entities.City;
import entities.Flight;
import entities.User;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class Main {

    public static void main(String[] args) {
        try {
            Configuration configuration = new Configuration().configure("hibernate.cfg.xml");
            configuration.addAnnotatedClass(User.class);
            configuration.addAnnotatedClass(City.class);
            configuration.addAnnotatedClass(Flight.class);
            ServiceRegistry serviceRegistry
                    = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties()).build();
            SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            FlightDAO flightDAO = new FlightDAO(sessionFactory);
            City arrival = new City("New York",23,23);
            City departure = new City("Chicago",50,50);
            Flight flight = new Flight(1234,"Boeing",departure,Timestamp.valueOf(LocalDateTime.now()),arrival
            ,Timestamp.valueOf(LocalDateTime.now()));
            flightDAO.createFlight(flight);
        } catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }

    }
}
