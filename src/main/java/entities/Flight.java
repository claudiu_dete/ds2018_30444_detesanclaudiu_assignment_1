package entities;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name="flight")
public class Flight {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="flight_number")
    private int flightNumber;

    @Column(name="airplane_type")
    private String airplaneType;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="departure_city_id")
    private City departureCity;

    @Column(name="departure_time")
    private Timestamp departureTime;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="arrival_city_id")
    private City arrivalCity;

    @Column(name="arrival_time")
    private Timestamp arrivalTime;

    public Flight() {
    }

    public Flight(int flightNumber,String airplaneType, City departureCity, Timestamp departureTime, City arrivalCity, Timestamp arrivalTime) {
        this.airplaneType = airplaneType;
        this.departureCity = departureCity;
        this.departureTime = departureTime;
        this.arrivalCity = arrivalCity;
        this.arrivalTime = arrivalTime;
        this.flightNumber = flightNumber;
    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(int flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }

    public Timestamp getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Timestamp departureTime) {
        this.departureTime = departureTime;
    }

    public Timestamp getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Timestamp arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public City getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(City departureCity) {
        this.departureCity = departureCity;
    }

    public City getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(City arrivalCity) {
        this.arrivalCity = arrivalCity;
    }
}
